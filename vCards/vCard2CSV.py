#!/usr/bin/env python3

# Copyright 2019 María Andrea Vignau

# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 3, as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranties of
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# For further info, check

__author__ = "María Andrea Vignau"

import csv
from pprint import pprint as pp
import vobject
class vCarReader:
    def __init__(self, filename):
        self.headers={}
        self.filename = filename
        self.discards = []
        self.load_cards()
        self.discard_no_data()
        self.see_duplicate_names()
        self.see_duplicate_phones()
        self.discard_duplicate_data()

    def discard_no_data(self):
        for idx, card in self.read_cards():
            phones = self.phone_dir(card)
            name = self.card_name(card).upper()
            if not name:
                self.discards.append(idx)

    def see_duplicate_names(self):
        self.names = {}
        self.no_phone = []
        for idx, card in self.read_cards():
            phones = self.phone_dir(card)
            name = self.card_name(card).upper()
            if not name in self.names:
                self.names[name] = [(phones, idx)]
            else:
                self.names[name] += [(phones, idx)]

    def see_duplicate_phones(self):
        self.phones = {}
        self.no_phone = []
        for idx, card in self.read_cards():
            #self.see_headers(card)
            if not 'tel' in card:
                self.add_no_phone(card)
            else:
                phones = self.phone_dir(card)
                for nmb in phones:
                    name = self.card_name(card)
                    if not nmb in self.phones:
                        self.phones[nmb] = [(name, idx)]
                    elif name not in self.phones[nmb]:
                        self.phones[nmb] += [(name, idx)]
        #self.list_duplicate_phones()
        #print(len(self.no_phone), self.no_phone)

    def list_duplicate_phones(self):
        for e, names in self.phones.items():
            if len(names) > 1:
                print(e, names)

    def add_no_phone(self, card):
        name = self.card_name(card).upper()
        if name and not name in self.no_phone:
            self.no_phone += [name]

    def card_name(self, card):
        if 'n' in card:
            k = 'n'
        elif 'fn' in card:
            k = 'fn'
        else:
            return ''
        name = str(card[k][0].value).strip()
        name = name.replace("  ", " ")
        return name

    def discard_duplicate_data(self):
        for e, phones in self.names.items():
            if len(phones) > 1:
                nd = self.discard_dup(phones)

        for e, names in self.phones.items():
            if len(names) > 1:
                nd = self.discard_dup(names)


    def cont(self, value):
        return self.cards[value[-1]].contents

    def discard_dup(self, values):
        l = [values[0]]
        if len(values) == 1:
            return l

        for v in values[1:]:
            if self.cont(v) == self.cont(l[0]):
                self.discards.append(v[-1])
            else:
                l.append(v)
        return l

    def phone_dir(self, card):
        rt = []
        if not 'tel' in card:
            return rt
        for phone in card["tel"]:
            p = str(phone.value)
            p = p.replace("-", "")
            if len(p) > 3:
                if not p.startswith('+'):
                    p = "+54" + p
            if not p in rt:
                rt += [p]
        return rt

    def load_cards(self):
        with open(self.filename, 'r') as inp:
            v = vobject.readComponents(inp, allowQP=True)
            self.cards = [card for card in v]

    def read_cards(self):
        for idx, vv in enumerate(self.cards):
            if not idx in self.discards:
                yield idx, vv.contents

    def see_headers(self, card):
        """
        data={}
        for a in dir(vv):
            if not a.startswith("_"):
                data[a] = getattr(vv, a, None)
        """
        #pp(vv.contents)
        for k, v in card.items():
            agre = [len(v), len(v), [str(a.value) for a in v]]
            #print(self.headers.keys(), k, agre)
            if k in self.headers:
                self.headers[k][0] = max(self.headers[k][0], agre[0])
                self.headers[k][1] += agre[1]
                self.headers[k][2].extend(agre[2])
            else:
                self.headers[k] = agre
                #print(k,self.headers[k])

    def print_headers(self):
        for k, v in self.headers.items():
            print(k, v[0], v[1], v[2:min(5, len(v))])

o = vCarReader("00001.vcf")
